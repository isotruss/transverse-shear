# ------------------------------------------------------
# These equations are based off the analysis done by Joseph Phillips
# There are a few parts of the thesis that is difficult to follow, so the following assumptions are made:
# 1 - V = M/L, we are assuming L = total length of the tower (see equation 4.39)
# 2 - Instead of using Aoh or Aih in equations 4.40-4.47, I use Al (area of longi). He uses Al in his
#     earlier equations and then switches with no explanation as to why.
# 3 - There are only lateral forces coming from one direction


import math
import numpy as np

# here is what is getting passed:
# materialInfo = [E1, filamentDiameter, fibersPerTow, vf]
towerInfo = [120, 27, 15, 0.35, 0.2, 1000, 2, 1200]
# towerInfo = [height (ft), diameter, bayLength, longiDia, heliDia, I (Gets Reset), loadFactor, totalHeight]
M = 10000       #kip*in
T = 10000        #kip*in

# Geometry Calculations
ro = towerInfo[1] / 2
ri = ro * 0.76536696388
Al = math.pi * 0.25 * towerInfo[3]**2
towerInfo[5] = 4 * Al * (towerInfo[1] / 2) ** 2  # Total moment of inertia

sinPi4 = math.sin(math.pi / 4)
sinPi8 = math.sin(math.pi / 8)
cosPi8 = math.cos(math.pi / 8)

y1 = ro * sinPi8
y2 = ri * sinPi4
y3 = ro * math.sin((3 * math.pi) / 8)
y4 = ri

Lb = towerInfo[2]
L1 = 2 * ro * sinPi8
L2 = (ro ** 2 + ri ** 2 - (2 * ro * ri * cosPi8)) ** (1 / 2)
L3 = 2 * ri * sinPi8
A1 = 0.5 * ro ** 2 * sinPi4
A2 = 0.5 * ro * ri * sinPi8
A3 = 0.5 * ri ** 2 * sinPi4
Aoc = A1 - 2 * A2
Amc = 2 * A2 - A3
Acc = 8 * A3
thetaBo = math.atan(Lb / L1)
Lo = L1 / math.cos(thetaBo)
Aoh = math.pi * 0.25 * towerInfo[4] ** 2  # Cross sectional area of outer helicals
Aih = math.pi * 0.25 * towerInfo[4] ** 2  # Cross sectional area of inner helicals
thetaB = math.atan(Lb / (2 * ro * sinPi4))

t1 = (2 * Aoh) / (Lb * math.cos(thetaBo))  # Effective thickness of the shear transferring outer web
t2 = (2 * Aih) / (Lb * math.cos(thetaB))  # Effective thickness of the shear transferring helical node web
t3 = t2  # Effective thickness of the shear transferring helical inner web

# Forces in the longitudinal members
Vy = M/towerInfo[7]
Vz = 0                  # we are assuming the lateral forces are only in the y direction

p1y = Vy * y1 * Al / towerInfo[5]
p2y = Vy * y2 * Al / towerInfo[5]
p3y = Vy * y3 * Al / towerInfo[5]
p4y = Vy * y4 * Al / towerInfo[5]
p1z = Vz * y1 * Al / towerInfo[5]
p2z = Vz * y2 * Al / towerInfo[5]
p3z = Vz * y3 * Al / towerInfo[5]
p4z = Vz * y4 * Al / towerInfo[5]

# Creating the 32x32 matrix of constants
A = np.zeros((32, 32))

for i in range(0,15):
    A[i,i+8] = 1
    A[i,i+9] = -1

    if i%2 == 0:                                            # This just checks if i is even
        A[i,int(i/2)] = 1
        col = (i/2) + 1
        A[i,int(col)] = -1

    else:                                                   # This will be if i is odd
        col = math.floor(i/2) + 24
        A[i,int(col)] = 1
        col = math.floor(i/2) + 25
        A[i,int(col)] = -1

for j in range(0,8):
    A[15,j] = 2 * A1
for j in range(8,24):
    A[15,j] = 2 * A2
for j in range(24,32):
    A[15,j] = 2 * A3

for i in range(16, 32):
    for j in range(24, 32):
        A[i,j] = - L3 / (2 * Acc *t3)

    if i%2 == 0:                                             # This checks if i is even
        col = (i/2) - 8
        if t1 == 0:
            A[i, int(col)] = 0
        else:
            A[i,int(col)] = L1 / (2 * Aoc * t1)

        if i > 16:                                           # there is a pattern that has one exception, if i = 16
            A[i,i-9] = -L2 / (2 * Aoc * t2)
            A[i,i-8] = -L2 / (2 * Aoc * t2)
        else:
            A[i,23] = -L2 / (2 * Aoc * t2)
            A[i,i-8] = -L2 / (2 * Aoc * t2)

    else:               # This is if i is odd
        col = math.floor(i/2) + 16
        A[i,int(col)] = A[i,int(col)] - (L3 / (2 * Amc * t3))
        A[i,i-9] = L2 / (2 * Amc * t2)
        A[i,i-8] = L2 / (2 * Amc * t2)

# Creating the B matrix:
temp = (-p1y - p3z, -p2y - p2z, -p3y - p1z, -p4y,
      -p3y + p1z, -p2y + p2z, -p1y + p3z, p4z,
      p1y + p3z, p2y + p2z, p3y + p1z, p4y,
      p3y - p1z, p2y - p2z, p1y - p3z, T)
B = np.zeros((32,1))
for i in range(16):
    B[i,0] = temp[i]

# Doing the matrix multiplication to find x
invA = np.linalg.inv(A)
# Solving for x, which will be called q for shear values
q = np.matmul(invA,B)

# --------------------------------- Converting shear into axial forces -----------------
################# For the outer web
# Resultant forces, shear forces, and axial forces
Fr = np.zeros(32)
Fv = np.zeros(32)
Fa = np.zeros(32)
# This for loop converts q1 shear values into resultant forces
for i in range(0,8):
    Fr[i] = (q[i]/2) * math.sqrt(L1**2 + Lb**2)

# These calculate the angle of forces
theta1 = math.atan(Lb / L1)
alpha1 = abs(thetaBo - theta1)

# This converts the resultant force into shear and axial forces
for i in range(0,8):
    Fv[i] = Fr[i]*math.sin(alpha1)
    Fa[i] = Fr[i]*math.cos(alpha1)

################ For the transverse web
# This for loop converts q1 shear values into resultant forces
for i in range(8, 24):
    Fr[i] = (q[i] / 2) * math.sqrt(L2 ** 2 + Lb ** 2)

# These calculate the angle of forces
theta2 = math.atan(Lb / L2)
alpha2 = abs(thetaB - theta2)

# This converts the resultant force into shear and axial forces
for i in range(8, 24):
    Fv[i] = Fr[i] * math.sin(alpha2)
    Fa[i] = Fr[i] * math.cos(alpha2)

################# For the inner web
# This for loop converts q1 shear values into resultant forces
for i in range(24,32):
    Fr[i] = (q[i]/2) * math.sqrt(L3**2 + Lb**2)

# These calculate the angle of forces
theta3 = math.atan(Lb / L3)
alpha3 = abs(thetaB - theta3)

# This converts the resultant force into shear and axial forces
for i in range(24,32):
    Fv[i] = Fr[i]*math.sin(alpha3)
    Fa[i] = Fr[i]*math.cos(alpha3)

print("Fa")
